<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name></name>
    <message id="Ambience Admirer">
        <location filename="../qml/harbour-ambienceadmirer.qml" line="38"/>
        <source>Ambience Admirer</source>
        <extracomment>name displayed on cover</extracomment>
        <translation>Ambientenbewunderer</translation>
    </message>
</context>
</TS>
