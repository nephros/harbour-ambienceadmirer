import QtQuick 2.1
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0

ApplicationWindow {
	ConfigurationValue {
		id: devicemodel
		key: "/desktop/lipstick-jolla-home/model"
	}

	id: app
	allowedOrientations: (devicemodel === 'planetgemini') ?  Orientation.LandscapeInverted : defaultAllowedOrientations
	initialPage: Component {
		Page {
			id: page
			allowedOrientations: (devicemodel === 'planetgemini') ?  Orientation.LandscapeInverted : defaultAllowedOrientations
			Image {
				cache: true
				source: Theme._homeBackgroundImage
				height: screen.heigt
				anchors.fill: parent
				fillMode: Image.PreserveAspectCrop
			}
		}
	}
	cover: Component {
		CoverBackground {
			CoverPlaceholder {
				anchors.fill: parent
				anchors.centerIn: parent
				icon.source: "image://theme/icon-m-ambience"
				//: name displayed on cover
				//% "Ambience Admirer"
				text: qsTrId("Ambience Admirer")
			}
		}
	}
}

